var notes = (function() {
    var list = [];

    return {
        add: function(note) {
            if (note) {
                var test = String(note);
                if (test.trim()) {
                    var item = {timestamp: Date.now(), text: note};
                    list.push(item);
                    return true;
                }
            }
            return false;
        },
        remove: function(index) {
            if (list) {
                list.splice(index, 1);
                return true;
            }
            return false;
        },
        count: function() {
            return list.length;
        },
        list: function() {},
        find: function(str) {
            if (list) {
                for (var i = 0; i < list.length; i++) {
                    if (list[i].text.indexOf(str) != -1) {
                        return true;
                    }
                }
            }
            return false;
        },
        clear: function() {
            list.splice(0, list.length);
        }
    }
}());