
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

var heading = document.createElement('P');
document.body.appendChild(heading);

var headings = document.createElement('tr');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var head = document.createElement('th');
	head.innerHTML = 'Heading ' + (i+1);
	headings.appendChild(head);
}
document.body.appendChild(headings);

var list = document.createElement('tr');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('td');
	item.innerHTML = books[i].title + ', published ' + books[i].year;
	list.appendChild(item);
}
document.body.appendChild(list);

var items = document.querySelectorAll('td');

items[0].onclick = function() {
	heading.innerHTML = books[0].title;
};
items[1].onclick = function() {
	heading.innerHTML = books[1].title;
};
items[2].onclick = function() {
	heading.innerHTML = books[2].title;
};
items[3].onclick = function() {
	heading.innerHTML = books[3].title;
};
items[4].onclick = function() {
	heading.innerHTML = books[4].title;
};