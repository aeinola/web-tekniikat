<?php

class Dice {
    protected $faces;
    protected $freqs = array();
    protected $sum;
    protected $throws;
    
    // Constructor
    public function __construct($faces) {
        $this->faces = $faces;
    }
    
    public function cast($bias) {
        if ($bias){
            if (rand(1,100) < $bias*100) {
                $this->freqs[$this->faces]++;
                return $this->faces;
            } else {
                $res = rand(1,$this->faces-1);
                $this->freqs[$res]++;
                return $res;
            }
        }
        $res = rand(1,$this->faces);
        $this->freqs[$res]++;
        return $res;
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    
    public function getAvg($val) {
        foreach ($val as $value) {
            $this->sum += $value;
        }
        return $this->sum / count($val);
    }
}

class PhysicalDice extends Dice {
    private $material;
    
    // Constructor
    public function __construct($faces, $material) {
        $this->faces = $faces;
        $this->material = $material;
    }
}

?>