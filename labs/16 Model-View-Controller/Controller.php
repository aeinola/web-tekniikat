<?php
    $nickname = '';
    class Controller {
        private $model;

        public function __construct() {
            $this->model = new Model();
        }

        public function list_it() {
            $this->messages = $this->model->messages();
            include("View.php");
        }

        public function send() {
            if (isset($_POST["nickname"])) {
                $nickname = $_POST["nickname"];
            }
            $this->model->add_message($_POST["message"], $nickname);
            header("Location: Chat.php?action=list_it&nickname=".$nickname);
        }
    }
?>