<?php
    $myArray = array("Akseli", "Matti", "Oskari");
    echo '<p><b><u>Array test:</u></b></p>';
    for ($i = 0; $i < count($myArray); $i++) {
        echo $myArray[$i] . '<br>';
    }
    
    echo '<p><b><u>Calculating area of a circle with radius of 12cm:</u></b></p>';
    echo 'Area of the circle is: ' . pi() * pow(12, 2) . 'cm^2';
?>